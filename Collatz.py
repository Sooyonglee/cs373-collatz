#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List
import math

# metacache indexes 1 to 1000, 1001 to 2000, etc.
# metacache = [0] * 1000
metacache = [179, 182, 217, 238, 215, 236, 262, 252, 247, 260, 268, 250, 263, 276, 271]
metacache.extend(
    [271, 266, 279, 261, 274, 256, 269, 269, 282, 264, 264, 308, 259, 259, 272]
)
metacache.extend(
    [272, 285, 267, 267, 311, 324, 249, 306, 244, 306, 288, 257, 288, 270, 270]
)
metacache.extend(
    [314, 283, 314, 296, 296, 278, 309, 340, 322, 260, 260, 322, 304, 273, 304]
)
metacache.extend(
    [335, 317, 286, 330, 299, 268, 268, 312, 312, 299, 312, 325, 263, 294, 325]
)
metacache.extend(
    [307, 307, 351, 338, 307, 320, 320, 320, 289, 320, 302, 302, 333, 333, 315]
)
metacache.extend(
    [315, 333, 315, 284, 315, 328, 297, 297, 284, 328, 341, 310, 310, 248, 310]
)
metacache.extend(
    [341, 354, 292, 279, 310, 292, 323, 323, 292, 305, 349, 305, 305, 336, 305]
)
metacache.extend(
    [318, 336, 318, 331, 287, 318, 331, 287, 331, 344, 331, 300, 331, 313, 300]
)
metacache.extend(
    [344, 313, 331, 313, 313, 344, 326, 375, 282, 326, 295, 357, 295, 326, 326]
)
metacache.extend(
    [370, 295, 308, 308, 352, 308, 383, 339, 321, 352, 370, 290, 339, 321, 334]
)
metacache.extend([321, 352, 321, 321, 334])
metacache.extend(
    [290, 334, 303, 347, 334, 272, 334, 334, 347, 303, 365, 316, 334, 254, 316]
)
metacache.extend([0] * 815)
metacache[185] = 329
metacache[186] = 347
metacache[187] = 329
metacache[188] = 316
metacache[189] = 360
metacache[190] = 329
metacache[191] = 329
metacache[192] = 347
metacache[193] = 329
metacache[194] = 342
metacache[195] = 360
metacache[196] = 298
metacache[197] = 285
metacache[198] = 329
metacache[199] = 329
metacache[200] = 342
metacache[201] = 311
metacache[202] = 342
metacache[203] = 311
metacache[204] = 311
metacache[205] = 355
metacache[206] = 373
metacache[207] = 311
metacache[208] = 311
metacache[209] = 311
metacache[210] = 342
metacache[211] = 355
metacache[212] = 355
metacache[213] = 373
metacache[214] = 293
metacache[215] = 280
metacache[216] = 386
metacache[217] = 324
metacache[218] = 324
metacache[219] = 355
metacache[220] = 324
metacache[221] = 355
metacache[222] = 324
metacache[223] = 324
metacache[224] = 324
metacache[225] = 368
metacache[226] = 368
metacache[227] = 306
metacache[228] = 355
metacache[229] = 306
metacache[230] = 443
metacache[231] = 350
metacache[232] = 337
metacache[233] = 368
metacache[234] = 381
metacache[235] = 306
metacache[236] = 337
metacache[237] = 350
metacache[238] = 306
metacache[239] = 350
metacache[240] = 368
metacache[241] = 275
metacache[242] = 319
metacache[243] = 337
metacache[244] = 275
metacache[245] = 319
metacache[246] = 332
metacache[247] = 350
metacache[248] = 288
metacache[249] = 350
metacache[250] = 332
metacache[251] = 319
metacache[252] = 319
metacache[253] = 332
metacache[254] = 363
metacache[255] = 288
metacache[256] = 332
metacache[257] = 345
metacache[258] = 301
metacache[259] = 345
metacache[260] = 332
metacache[261] = 332
metacache[262] = 301
metacache[263] = 407
metacache[264] = 332
metacache[265] = 332
metacache[266] = 314
metacache[267] = 345
metacache[268] = 270
metacache[269] = 345
metacache[270] = 407
metacache[271] = 283
metacache[272] = 314
metacache[273] = 358
metacache[274] = 332
metacache[275] = 345
metacache[276] = 314
metacache[277] = 389
metacache[278] = 345
metacache[279] = 314
metacache[280] = 345
metacache[281] = 358
metacache[282] = 314
metacache[283] = 358
metacache[284] = 358
metacache[285] = 376
metacache[286] = 314
metacache[287] = 327
metacache[288] = 389
metacache[289] = 345
metacache[290] = 327
metacache[291] = 327
metacache[292] = 340
metacache[293] = 358
metacache[294] = 296
metacache[295] = 358
metacache[296] = 327
metacache[297] = 327
metacache[298] = 371
metacache[299] = 327
metacache[300] = 371
metacache[301] = 296
metacache[302] = 340
metacache[303] = 340
metacache[304] = 340
metacache[305] = 265
metacache[306] = 309
metacache[307] = 309
metacache[308] = 371
metacache[309] = 340
metacache[310] = 371
metacache[311] = 309
metacache[312] = 384
metacache[313] = 340
metacache[314] = 278
metacache[315] = 340
metacache[316] = 353
metacache[317] = 309
metacache[318] = 353
metacache[319] = 322
metacache[320] = 371
metacache[321] = 353
metacache[322] = 309
metacache[323] = 322
metacache[324] = 384
metacache[325] = 340
metacache[326] = 247
metacache[327] = 322
metacache[328] = 291
metacache[329] = 353
metacache[330] = 322
metacache[331] = 291
metacache[332] = 353
metacache[333] = 335
metacache[334] = 322
metacache[335] = 322
metacache[336] = 366
metacache[337] = 366
metacache[338] = 335
metacache[339] = 366
metacache[340] = 304
metacache[341] = 335
metacache[342] = 353
metacache[343] = 335
metacache[344] = 304
metacache[345] = 441
metacache[346] = 348
metacache[347] = 322
metacache[348] = 335
metacache[349] = 366
metacache[350] = 304
metacache[351] = 379
metacache[352] = 335
metacache[353] = 304
metacache[354] = 348
metacache[355] = 379
metacache[356] = 348
metacache[357] = 304
metacache[358] = 379
metacache[359] = 348
metacache[360] = 410
metacache[361] = 348
metacache[362] = 361
metacache[363] = 317
metacache[364] = 317
metacache[365] = 361
metacache[366] = 348
metacache[367] = 286
metacache[368] = 317
metacache[369] = 361
metacache[370] = 392
metacache[371] = 348
metacache[372] = 317
metacache[373] = 348
metacache[374] = 330
metacache[375] = 361
metacache[376] = 423
metacache[377] = 361
metacache[378] = 330
metacache[379] = 361
metacache[380] = 379
metacache[381] = 374
metacache[382] = 361
metacache[383] = 330
metacache[384] = 330
metacache[385] = 348
metacache[386] = 330
metacache[387] = 299
metacache[388] = 330
metacache[389] = 436
metacache[390] = 361
metacache[391] = 330
metacache[392] = 299
metacache[393] = 361
metacache[394] = 405
metacache[395] = 312
metacache[396] = 330
metacache[397] = 330
metacache[398] = 374
metacache[399] = 299
metacache[400] = 374
metacache[401] = 387
metacache[402] = 268
metacache[403] = 343
metacache[404] = 343
metacache[405] = 405
metacache[406] = 361
metacache[407] = 268
metacache[408] = 312
metacache[409] = 312
metacache[410] = 449
metacache[411] = 330
metacache[412] = 343
metacache[413] = 374
metacache[414] = 374
metacache[415] = 312
metacache[416] = 387
metacache[417] = 343
metacache[418] = 343
metacache[419] = 281
metacache[420] = 343
metacache[421] = 325
metacache[422] = 356
metacache[423] = 418
metacache[424] = 356
metacache[425] = 356
metacache[426] = 356
metacache[427] = 374
metacache[428] = 294
metacache[429] = 281
metacache[430] = 312
metacache[431] = 343
metacache[432] = 387
metacache[433] = 343
metacache[434] = 356
metacache[435] = 281
metacache[436] = 325
metacache[437] = 387
metacache[438] = 400
metacache[439] = 356
metacache[440] = 325
metacache[441] = 294
metacache[442] = 356
metacache[443] = 338
metacache[444] = 325
metacache[445] = 338
metacache[446] = 325
metacache[447] = 325
metacache[448] = 369
metacache[449] = 369
metacache[450] = 387
metacache[451] = 307
metacache[452] = 294
metacache[453] = 369
metacache[454] = 338
metacache[455] = 338
metacache[456] = 356
metacache[457] = 338
metacache[458] = 307
metacache[459] = 307
metacache[460] = 307
metacache[461] = 444
metacache[462] = 369
metacache[463] = 325
metacache[464] = 338
metacache[465] = 369
metacache[466] = 369
metacache[467] = 413
metacache[468] = 382
metacache[469] = 338
metacache[470] = 307
metacache[471] = 276
metacache[472] = 338
metacache[473] = 307
metacache[474] = 382
metacache[475] = 320
metacache[476] = 307
metacache[477] = 382
metacache[478] = 351
metacache[479] = 351
metacache[480] = 413
metacache[481] = 382
metacache[482] = 351
metacache[483] = 307
metacache[484] = 320
metacache[485] = 338
metacache[486] = 382
metacache[487] = 382
metacache[488] = 382
metacache[489] = 351
metacache[490] = 320
metacache[491] = 320
metacache[492] = 426
metacache[493] = 395
metacache[494] = 351
metacache[495] = 320
metacache[496] = 320
metacache[497] = 289
metacache[498] = 351
metacache[499] = 395
metacache[500] = 364
metacache[501] = 320
metacache[502] = 426
metacache[503] = 320
metacache[504] = 364
metacache[505] = 364
metacache[506] = 382
metacache[507] = 364
metacache[508] = 377
metacache[509] = 364
metacache[510] = 333
metacache[511] = 470
metacache[512] = 333
metacache[513] = 351
metacache[514] = 364
metacache[515] = 395
metacache[516] = 302
metacache[517] = 333
metacache[518] = 439
metacache[519] = 364
metacache[520] = 333
metacache[521] = 364
metacache[522] = 333
metacache[523] = 302
metacache[524] = 364
metacache[525] = 408
metacache[526] = 408
metacache[527] = 377
metacache[528] = 377
metacache[529] = 333
metacache[530] = 346
metacache[531] = 346
metacache[532] = 377
metacache[533] = 377
metacache[534] = 346
metacache[535] = 302
metacache[536] = 333
metacache[537] = 377
metacache[538] = 346
metacache[539] = 346
metacache[540] = 408
metacache[541] = 364
metacache[542] = 346
metacache[543] = 359
metacache[544] = 315
metacache[545] = 346
metacache[546] = 452
metacache[547] = 377
metacache[548] = 333
metacache[549] = 315
metacache[550] = 346
metacache[551] = 377
metacache[552] = 315
metacache[553] = 346
metacache[554] = 421
metacache[555] = 390
metacache[556] = 346
metacache[557] = 315
metacache[558] = 315
metacache[559] = 284
metacache[560] = 359
metacache[561] = 328
metacache[562] = 359
metacache[563] = 328
metacache[564] = 421
metacache[565] = 328
metacache[566] = 359
metacache[567] = 359
metacache[568] = 359
metacache[569] = 359
metacache[570] = 377
metacache[571] = 359
metacache[572] = 372
metacache[573] = 359
metacache[574] = 328
metacache[575] = 346
metacache[576] = 390
metacache[577] = 390
metacache[578] = 346
metacache[579] = 359
metacache[580] = 266
metacache[581] = 328
metacache[582] = 328
metacache[583] = 434
metacache[584] = 372
metacache[585] = 359
metacache[586] = 359
metacache[587] = 328
metacache[588] = 315
metacache[589] = 297
metacache[590] = 359
metacache[591] = 403
metacache[592] = 328
metacache[593] = 328
metacache[594] = 328
metacache[595] = 328
metacache[596] = 372
metacache[597] = 372
metacache[598] = 341
metacache[599] = 372
metacache[600] = 372
metacache[601] = 403
metacache[602] = 297
metacache[603] = 297
metacache[604] = 372
metacache[605] = 341
metacache[606] = 328
metacache[607] = 341
metacache[608] = 403
metacache[609] = 359
metacache[610] = 341
metacache[611] = 354
metacache[612] = 310
metacache[613] = 310
metacache[614] = 310
metacache[615] = 447
metacache[616] = 372
metacache[617] = 372
metacache[618] = 310
metacache[619] = 341
metacache[620] = 372
metacache[621] = 372
metacache[622] = 310
metacache[623] = 354
metacache[624] = 385
metacache[625] = 354
metacache[626] = 509
metacache[627] = 341
metacache[628] = 279
metacache[629] = 279
metacache[630] = 341
metacache[631] = 310
metacache[632] = 385
metacache[633] = 354
metacache[634] = 310
metacache[635] = 416
metacache[636] = 310
metacache[637] = 354
metacache[638] = 354
metacache[639] = 354
metacache[640] = 416
metacache[641] = 372
metacache[642] = 416
metacache[643] = 279
metacache[644] = 279
metacache[645] = 310
metacache[646] = 323
metacache[647] = 341
metacache[648] = 323
metacache[649] = 385
metacache[650] = 341
metacache[651] = 336
metacache[652] = 354
metacache[653] = 385
metacache[654] = 323
metacache[655] = 385
metacache[656] = 429
metacache[657] = 354
metacache[658] = 398
metacache[659] = 354
metacache[660] = 323
metacache[661] = 323
metacache[662] = 292
metacache[663] = 292
metacache[664] = 367
metacache[665] = 442
metacache[666] = 323
metacache[667] = 367
metacache[668] = 323
metacache[669] = 336
metacache[670] = 323
metacache[671] = 323
metacache[672] = 367
metacache[673] = 336
metacache[674] = 367
metacache[675] = 385
metacache[676] = 336
metacache[677] = 336
metacache[678] = 380
metacache[679] = 367
metacache[680] = 336
metacache[681] = 336
metacache[682] = 305
metacache[683] = 380
metacache[684] = 336
metacache[685] = 354
metacache[686] = 398
metacache[687] = 380
metacache[688] = 292
metacache[689] = 336
metacache[690] = 336
metacache[691] = 442
metacache[692] = 323
metacache[693] = 367
metacache[694] = 367
metacache[695] = 336
metacache[696] = 336
metacache[697] = 318
metacache[698] = 367
metacache[699] = 367
metacache[700] = 367
metacache[701] = 411
metacache[702] = 380
metacache[703] = 349
metacache[704] = 504
metacache[705] = 380
metacache[706] = 305
metacache[707] = 349
metacache[708] = 349
metacache[709] = 336
metacache[710] = 380
metacache[711] = 380
metacache[712] = 411
metacache[713] = 380
metacache[714] = 305
metacache[715] = 305
metacache[716] = 380
metacache[717] = 349
metacache[718] = 336
metacache[719] = 349
metacache[720] = 411
metacache[721] = 367
metacache[722] = 411
metacache[723] = 349
metacache[724] = 362
metacache[725] = 305
metacache[726] = 349
metacache[727] = 318
metacache[728] = 362
metacache[729] = 318
metacache[730] = 380
metacache[731] = 336
metacache[732] = 380
metacache[733] = 349
metacache[734] = 349
metacache[735] = 424
metacache[736] = 318
metacache[737] = 380
metacache[738] = 424
metacache[739] = 424
metacache[740] = 393
metacache[741] = 318
metacache[742] = 349
metacache[743] = 318
metacache[744] = 318
metacache[745] = 349
metacache[746] = 287
metacache[747] = 362
metacache[748] = 318
metacache[749] = 393
metacache[750] = 362
metacache[751] = 331
metacache[752] = 318
metacache[753] = 424
metacache[754] = 318
metacache[755] = 362
metacache[756] = 362
metacache[757] = 331
metacache[758] = 362
metacache[759] = 362
metacache[760] = 380
metacache[761] = 362
metacache[762] = 331
metacache[763] = 375
metacache[764] = 362
metacache[765] = 331
metacache[766] = 331
metacache[767] = 468
metacache[768] = 331
metacache[769] = 393
metacache[770] = 349
metacache[771] = 344
metacache[772] = 393
metacache[773] = 362
metacache[774] = 331
metacache[775] = 437
metacache[776] = 331
metacache[777] = 393
metacache[778] = 437
metacache[779] = 362
metacache[780] = 344
metacache[781] = 362
metacache[782] = 331
metacache[783] = 331
metacache[784] = 313
metacache[785] = 375
metacache[786] = 300
metacache[787] = 362
metacache[788] = 406
metacache[789] = 406
metacache[790] = 375
metacache[791] = 344
metacache[792] = 437
metacache[793] = 375
metacache[794] = 331
metacache[795] = 331
metacache[796] = 468
metacache[797] = 344
metacache[798] = 300
metacache[799] = 375
metacache[800] = 375
metacache[801] = 406
metacache[802] = 388
metacache[803] = 300
metacache[804] = 331
metacache[805] = 375
metacache[806] = 344
metacache[807] = 406
metacache[808] = 331
metacache[809] = 344
metacache[810] = 406
metacache[811] = 287
metacache[812] = 362
metacache[813] = 375
metacache[814] = 313
metacache[815] = 357
metacache[816] = 344
metacache[817] = 344
metacache[818] = 450
metacache[819] = 357
metacache[820] = 450
metacache[821] = 375
metacache[822] = 344
metacache[823] = 375
metacache[824] = 326
metacache[825] = 344
metacache[826] = 313
metacache[827] = 419
metacache[828] = 313
metacache[829] = 375
metacache[830] = 344
metacache[831] = 419
metacache[832] = 388
metacache[833] = 357
metacache[834] = 344
metacache[835] = 313
metacache[836] = 344
metacache[837] = 525
metacache[838] = 344
metacache[839] = 388
metacache[840] = 357
metacache[841] = 313
metacache[842] = 388
metacache[843] = 331
metacache[844] = 357
metacache[845] = 401
metacache[846] = 313
metacache[847] = 419
metacache[848] = 313
metacache[849] = 357
metacache[850] = 357
metacache[851] = 357
metacache[852] = 326
metacache[853] = 357
metacache[854] = 419
metacache[855] = 388
metacache[856] = 375
metacache[857] = 357
metacache[858] = 370
metacache[859] = 313
metacache[860] = 357
metacache[861] = 326
metacache[862] = 326
metacache[863] = 344
metacache[864] = 326
metacache[865] = 388
metacache[866] = 326
metacache[867] = 344
metacache[868] = 357
metacache[869] = 388
metacache[870] = 326
metacache[871] = 432
metacache[872] = 326
metacache[873] = 326
metacache[874] = 388
metacache[875] = 432
metacache[876] = 370
metacache[877] = 401
metacache[878] = 326
metacache[879] = 357
metacache[880] = 326
metacache[881] = 326
metacache[882] = 313
metacache[883] = 370
metacache[884] = 295
metacache[885] = 370
metacache[886] = 445
metacache[887] = 401
metacache[888] = 295
metacache[889] = 370
metacache[890] = 339
metacache[891] = 326
metacache[892] = 295
metacache[893] = 326
metacache[894] = 370
metacache[895] = 326
metacache[896] = 370
metacache[897] = 339
metacache[898] = 370
metacache[899] = 295
metacache[900] = 401
metacache[901] = 401
metacache[902] = 383
metacache[903] = 295
metacache[904] = 383
metacache[905] = 295
metacache[906] = 445
metacache[907] = 326
metacache[908] = 370
metacache[909] = 326
metacache[910] = 476
metacache[911] = 383
metacache[912] = 401
metacache[913] = 357
metacache[914] = 370
metacache[915] = 339
metacache[916] = 339
metacache[917] = 383
metacache[918] = 339
metacache[919] = 432
metacache[920] = 339
metacache[921] = 339
metacache[922] = 445
metacache[923] = 308
metacache[924] = 370
metacache[925] = 339
metacache[926] = 370
metacache[927] = 476
metacache[928] = 383
metacache[929] = 339
metacache[930] = 370
metacache[931] = 308
metacache[932] = 370
metacache[933] = 370
metacache[934] = 414
metacache[935] = 414
metacache[936] = 383
metacache[937] = 352
metacache[938] = 445
metacache[939] = 507
metacache[940] = 383
metacache[941] = 339
metacache[942] = 339
metacache[943] = 352
metacache[944] = 383
metacache[945] = 352
metacache[946] = 383
metacache[947] = 383
metacache[948] = 383
metacache[949] = 352
metacache[950] = 414
metacache[951] = 383
metacache[952] = 414
metacache[953] = 414
metacache[954] = 383
metacache[955] = 339
metacache[956] = 352
metacache[957] = 352
metacache[958] = 321
metacache[959] = 352
metacache[960] = 414
metacache[961] = 352
metacache[962] = 383
metacache[963] = 414
metacache[964] = 352
metacache[965] = 321
metacache[966] = 365
metacache[967] = 308
metacache[968] = 321
metacache[969] = 352
metacache[970] = 458
metacache[971] = 352
metacache[972] = 321
metacache[973] = 383
metacache[974] = 383
metacache[975] = 339
metacache[976] = 383
metacache[977] = 334
metacache[978] = 383
metacache[979] = 334
metacache[980] = 427
metacache[981] = 321
metacache[982] = 383
metacache[983] = 383
metacache[984] = 352
metacache[985] = 427
metacache[986] = 352
metacache[987] = 396
metacache[988] = 321
metacache[989] = 352
metacache[990] = 321
metacache[991] = 321
metacache[992] = 352
metacache[993] = 290
metacache[994] = 365
metacache[995] = 365
metacache[996] = 365
metacache[997] = 440
metacache[998] = 396

cache = [0] * 1000000
cache[1] = 1

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    assert i > 0
    assert j > 0
    assert i < 1000000
    assert j < 1000000

    small = i
    large = j
    if j < i:
        small = j
        large = i

    max_cycle_length = 1

    for x in range(math.ceil(small / 1000.0), math.floor(large / 1000.0)):
        if metacache[x] > max_cycle_length:
            max_cycle_length = metacache[x]

    large_front = min((math.ceil(small / 1000.0) * 1000), large)

    small_back = max(small, math.floor(large / 1000.0) * 1000)

    first_part = collatz_eval_helper(small, large_front)
    if first_part > max_cycle_length:
        max_cycle_length = first_part

    second_part = collatz_eval_helper(small_back, large)
    if second_part > max_cycle_length:
        max_cycle_length = second_part

    assert max_cycle_length > 0
    return max_cycle_length


# -------------------
# collatz_eval_helper
# -------------------


def collatz_eval_helper(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    max_cycle_length = 1
    for num in range(i, j + 1):
        cur_cycle_length = 1
        initial = num
        while num > 1:
            if (num % 2) == 0:
                num = num // 2
                if num < 1000000 and cache[num] > 0:
                    cur_cycle_length += cache[num]
                    break
            else:
                num = (3 * num + 1) // 2
                cur_cycle_length += 1
                if num < 1000000 and cache[num] > 0:
                    cur_cycle_length += cache[num]
                    break
            cur_cycle_length += 1
        cache[initial] = cur_cycle_length
        if cur_cycle_length > max_cycle_length:
            max_cycle_length = cur_cycle_length
    return max_cycle_length


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
